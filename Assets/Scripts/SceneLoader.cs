using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

	public string sceneToLoad;

	public void LoadScene () {
		SceneManager.LoadScene (sceneToLoad, LoadSceneMode.Single);
	}

	public void LoadScene (int id)
	{
		SceneManager.LoadScene(id, LoadSceneMode.Single);
	} 

	public void LoadScene(string name)
	{
		SceneManager.LoadScene(name, LoadSceneMode.Single);
	}

	public void RestartScene()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
	}
}