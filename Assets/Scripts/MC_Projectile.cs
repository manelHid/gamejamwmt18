﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MC_Projectile : MonoBehaviour {

    public int damage = 10;
    public float speed = 7.0f;

	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Ground")
        {
            Destroy(gameObject);
        }
        if (collision.gameObject.tag == "Enemy")
        {
            GameObject enemyObject = collision.transform.parent.gameObject;
            
            Object enemyMelee = collision.gameObject.GetComponent("EnemyMelee");
            Object enemyRange = collision.gameObject.GetComponent("EnemyRange");
            if (enemyMelee != null)
            {
                ((EnemyMelee)enemyMelee).TakeDamage(damage);
            }
            else if (enemyRange != null)
            {
                ((EnemyRange)enemyRange).TakeDamage(damage);
            }
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

}
