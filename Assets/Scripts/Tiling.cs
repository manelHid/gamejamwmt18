using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(SpriteRenderer))]


public class Tiling : MonoBehaviour {

	public int offsetX = 2;

	public bool hasRightBuddy = false;
	public bool hasLeftBuddy = false;

	public bool reverseScale = false;

	private float spriteWidth = 0f;
	private Camera cam;
	private Transform myTransform;

	void Awake ()
	{
		cam = Camera.main;
		myTransform = transform;
    }
	// Use this for initialization
	void Start () {
		SpriteRenderer sRenderer = GetComponent<SpriteRenderer>();
		spriteWidth = sRenderer.sprite.bounds.size.x;
	}
	
	// Update is called once per frame
	void Update () {
		if (!hasLeftBuddy || !hasRightBuddy)
		{
			float camHorizontalExtent = cam.orthographicSize * Screen.width / Screen.height;

			float edgeVisiblePositionRight = (myTransform.position.x + spriteWidth / 2) - camHorizontalExtent;
			float edgeVisiblePositionLeft = (myTransform.position.x - spriteWidth / 2) + camHorizontalExtent;

			if (cam.transform.position.x >= edgeVisiblePositionRight - offsetX && !hasRightBuddy)
			{
				//Debug.Log("Spawning right buddy");
				makeNewBuddy(1);
				hasRightBuddy = true;
			}
			else if (cam.transform.position.x <= edgeVisiblePositionLeft + offsetX && !hasLeftBuddy)
			{
				//Debug.Log("Spawning left buddy");
				makeNewBuddy(-1);
				hasLeftBuddy = true;
			}
		}
		
	}

	void makeNewBuddy(int rightOrLeft)
	{
		Vector3 newPos = new Vector3(myTransform.position.x + spriteWidth * rightOrLeft, myTransform.position.y, myTransform.position.z);
		Transform newBuddy = Instantiate(myTransform, newPos, myTransform.rotation) as Transform;

		if (reverseScale)
		{
			newBuddy.localScale = new Vector3(newBuddy.localScale.x * -1, newBuddy.localScale.y, newBuddy.localScale.z);
		}

		newBuddy.parent = myTransform.parent;
		if (rightOrLeft > 0)
		{
			newBuddy.GetComponent<Tiling>().hasLeftBuddy = true;
		}
		else
		{
			newBuddy.GetComponent<Tiling>().hasRightBuddy = true;
		}
	}
}
