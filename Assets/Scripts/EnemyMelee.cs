﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMelee : MonoBehaviour {

    public int hp = 10;
    public float velocity = 5f;

    private Rigidbody2D rigidbody2D;
    private List<GameObject> bounds = new List<GameObject>();
    private bool isFacingRight = true;

    public bool markedForDestroy = false;
    private AudioManager am;

    // Use this for initialization
    void Start () {
        rigidbody2D = GetComponent<Rigidbody2D>();
        am = AudioManager.instance;

       for (int i=1; i< transform.parent.gameObject.transform.childCount; i++)
            bounds.Add(transform.parent.gameObject.transform.GetChild(i).gameObject);
    }
	
	// Update is called once per frame
	void Update () {

        rigidbody2D.velocity = new Vector2(velocity, rigidbody2D.velocity.y);
        if(rigidbody2D.position.x < bounds[0].transform.position.x && !isFacingRight) 
        {
            Flip();
        }

        if (rigidbody2D.position.x > bounds[1].transform.position.x && isFacingRight)
        {
            Flip();
        }
    }

    void Flip()
    {
        Vector3 currentScale = rigidbody2D.transform.localScale;
        currentScale.x *= -1;
        rigidbody2D.transform.localScale = currentScale;
        velocity *= -1;
        isFacingRight = !isFacingRight;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Player player = collision.gameObject.GetComponent<Player>();
            player.TakeDamage(10);
        }
    }

    public void TakeDamage(int dmg)
    {
        hp = Mathf.Max(hp - Mathf.Max(dmg, 0), 0);

        if (hp <= 0)
        {
            am.PlaySound("EnemyRangeDeath");
            // play Death anim
            Animator anim = gameObject.GetComponent<Animator>();
            IEnumerator animCo = PlayAnimationAndWait(anim, "Death");
            StartCoroutine(animCo);
        }
    }

    private IEnumerator PlayAnimationAndWait(Animator anim, string animName)
    {
        anim.Play(animName);
        velocity = 0.0f;
        rigidbody2D.isKinematic = true;
        GameObject triggerObj = transform.GetChild(0).gameObject;
        triggerObj.GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length * 2.5f);
        Destroy(transform.parent.gameObject);
    }

}
