﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGroundedCollider : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ((Player) transform.parent.GetComponent("Player")).isGrounded = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        ((Player) transform.parent.GetComponent("Player")).isGrounded = false;
    }

}
