﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class clickExit : MonoBehaviour {

    public void clickToExit()
    {
		#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
		#endif
        Application.Quit();
    }
}
