﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRange : MonoBehaviour {

    public int hp = 10;
    public int shootingRate = 1; //Shots/second
    public bool flip = false;
    public Transform projectilePrefab;
    public bool doShoot = false;

    private Rigidbody2D rigidbody2D;
    private bool isFacingRight = false;
    public bool playerInRange = false;
    public float nextTimeToShoot;
    public float currentDeltaTime;

    public Animator anim;
    private AudioManager am;

    // Use this for initialization
    void Start () {
        rigidbody2D = GetComponent<Rigidbody2D>();
        nextTimeToShoot = Time.deltaTime;

        am = AudioManager.instance;

    }
	
	// Update is called once per frame
	void Update () {
        if (flip)
        {
            Flip();

            flip = false;
        }

        if (playerInRange)
        {
            if(Time.time > nextTimeToShoot)
            {
                Shoot();
                nextTimeToShoot = Time.time + (1 / shootingRate);
            }
        }

        if (doShoot)
        {
            Shoot();
            doShoot = false;
        }

        currentDeltaTime = Time.time;
    }

    private void Flip()
    {
        // Flip Animation
        Vector3 currentScale = transform.localScale;
        currentScale.x *= -1;
        transform.localScale = currentScale;
        isFacingRight = !isFacingRight;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Player player = collision.gameObject.GetComponent<Player>();
            if(player.transform.position.x < transform.position.x)
            {
                if (isFacingRight)
                    Flip();

            }else if(player.transform.position.x > transform.position.x)
            {
                if (!isFacingRight)
                    Flip();
            }


            playerInRange = true;
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerInRange = false;

        }
    }

    private void Shoot()
    {
        
        Transform projectile = Instantiate(projectilePrefab, transform.position, Quaternion.identity);
        Rigidbody2D projectile_rb2d = projectile.GetComponent<Rigidbody2D>();
        EnemyProjectile projectileComponent = (EnemyProjectile)projectile.GetComponent("EnemyProjectile");
        projectile_rb2d.velocity = new Vector2(projectileComponent.speed * (isFacingRight ? 1.0f : -1.0f), 0.0f);
        
    }

    public void TakeDamage(int dmg)
    {
        hp = Mathf.Max(hp - Mathf.Max(dmg, 0), 0);
        

        if(hp <= 0)
        {
            am.PlaySound("EnemyRangeDeath");
            // play Death anim
            anim.SetTrigger("Die");
            
        }
    }

    public void Die()
    {
        //GameObject triggerObj = transform.GetChild(0).gameObject;
        //triggerObj.GetComponent<Collider2D>().enabled = false;
        Destroy(transform.parent.gameObject);
    }

}
