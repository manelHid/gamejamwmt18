using UnityEngine;

public class Parallaxing : MonoBehaviour {

	public Transform[] backgrounds;     //Array of all the back and forgrounds to be parallax.
	private float[] parallaxScales;     //Array of proportions of camera movement to move the backgrounds.
	public float smoothing = 1;         //How smooth the parallax is going to be, set this bigger than 0;
    public float multiplier;            //Parallax effect multiplier. Bigger than 0

	private Transform cam;              //Reference to the camera.
	private Vector3 previousCamPos;		//Stores the previous frame camera position.

	void Awake ()
	{
		cam = Camera.main.transform;
		if (cam == null)
		{
			Debug.LogError("Parallaxing: Camera null!");
		}
	}

	// Use this for initialization
	void Start () {
		previousCamPos = cam.position;

		parallaxScales = new float[backgrounds.Length];

		for (int i = 0; i < parallaxScales.Length; i++)
		{
			parallaxScales[i] = backgrounds[i].position.z * -1 * multiplier;
            Debug.Log("Parallax scales " + i + ": " + parallaxScales[i]);
		}

		
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log("PlayerCamera Pos X: " + cam.position.x + " Pos Y: " + cam.position.y + " Pos Z: " + cam.position.z);
		for (int i = 0; i < backgrounds.Length; i++)
		{
			float parallax = (previousCamPos.x - cam.position.x) * parallaxScales[i];
			//Debug.Log("Parallax: " + parallax);

			float backgroundTargetPosX = backgrounds[i].position.x + parallax;

			Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, backgrounds[i].position.y, backgrounds[i].position.z);

			backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, smoothing * Time.deltaTime);
			//Debug.Log(backgrounds[i].position.ToString());
        }

		previousCamPos = cam.position;
		
	}
}
