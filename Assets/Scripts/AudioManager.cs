using UnityEngine;
using System.Collections;

[System.Serializable]
public class Sound
{
	#region Variables
	//Name of the sound clip
	public string name;

	//Audio clip/file
	public AudioClip clip;

	//The clip is a music?
	public bool isMusic = false;

	//Definition of volume, randomVolume, pitch and randomPitch between a specific range
	[Range(0f, 1f)]
	public float volume = 0.7f;
	[Range(0.5f, 1.5f)]
	public float pitch = 1f;

	[Range(0f, 0.5f)]
	public float randomVolume = 0.1f;
	[Range(0f, 0.5f)]
	public float randomPitch = 0.1f;

	//Do we want the audio clip to loop?
	public bool loop = false;

	//The AudioSource gameObject for this clip
	private AudioSource source;
	#endregion


	#region Methods
	public void SetSource(AudioSource _source)
	{
		source = _source;
		source.clip = clip;
	}

	public void Play()
	{
		source.loop = loop;
        source.mute = false;
        source.volume = volume * (1 + Random.Range(-randomVolume / 2f, randomVolume / 2f)) * 100;
		source.pitch = pitch * (1 + Random.Range(-randomPitch / 2f, randomPitch / 2f));
		//Debug.Log("Mute: " + source.mute + "\tVolume: " + source.volume + "\tPitch: " + source.pitch);
		//Debug.Log("PSMusicEnabled: " + PlayerSettings.instance.isMusicEnabled + "\tPSMusicVolume: " + PlayerSettings.instance.musicVolume);
		if (isMusic)
			source.Play();
		else
			source.PlayOneShot(clip);		
	}

	public void Stop()
	{
		source.Stop();
	}

	public void SetVolume(float _volume)
	{
		source.volume = _volume;
	}

	public void SetPitch(float _pitch)
	{
		pitch = _pitch;
		source.pitch = _pitch;
	}

	public bool mute
	{
		get { return source.mute; }
		set { source.mute = value; }
	}
	#endregion
}

public class AudioManager : MonoBehaviour
{

	#region Variables
	//The singleton instance of the AudioManager
	public static AudioManager instance;

	//Array of audio clips
	[SerializeField]
	Sound[] sounds;
	#endregion


	#region Unity Methods
	//Singleton of the AudioManager + DontDestroyOnLoad
	private void Awake()
	{
		if (instance != null)
		{
			if (instance != this)
			{
				//Debug.Log("AudioManager: Destroying the second instance of an audioManager.");
				Destroy(this.gameObject);
			}
		}
		else
		{
			instance = this;
		}		
	}

	//We create gameObjects with audio clips that we will use
	private void Start()
	{
		DontDestroyOnLoad(this);
		for (int i = 0; i < sounds.Length; i++)
		{
			GameObject _go = new GameObject("Sound_" + i + "_" + sounds[i].name);

			//We need to parent it even in mobile because they will be destroyed when scene switching occurs
			_go.transform.SetParent(this.transform);

			sounds[i].SetSource(_go.AddComponent<AudioSource>());
		}

        StopAllSounds();
        PlaySound("Menu");
	}

	/*private void OnDestroy()
	{
		Debug.Log("AudioManager: Destroyed.");
	}*/
	#endregion


	#region Methods
	public void PlaySound(string _name)
	{
		for (int i = 0; i < sounds.Length; i++)
		{
			if (sounds[i].name == _name)
			{
				sounds[i].Play();
				return;		
			}
		}
		//No sound with _name
		Debug.LogWarning("AudioManager: No sound found in array with name: " + _name);

	}

	public void StopSound(string _name)
	{
		for (int i = 0; i < sounds.Length; i++)
		{
			if (sounds[i].name == _name)
			{
				sounds[i].Stop();
				return;
			}
		}
		//No sound with _name
		Debug.LogWarning("AudioManager: No sound found in array with name: " + _name);

	}

    public void StopAllSounds()
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            sounds[i].Stop();
        }
    }
	#endregion
}
