﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;   

public class IngameUI : MonoBehaviour {

    #region Variables
    //Array of portraits
    [SerializeField]
    Sprite[] portraits = new Sprite[4];

    [SerializeField]
    Image portrait;

    [SerializeField]
    private RectTransform healthBarRect;

    [SerializeField]
    private float healthLerpSpeed = 1f;
    private float healthScale = 0f;
    private float healthScaleToLerp;
    private float scaleLerpValue;
    private float rate;

    float healthPoints;
    #endregion

    // Use this for initialization
    void Start () {
        for (int i = 0; i < portraits.Length; i++)
        {
            if (portraits[i] == null)
                Debug.LogWarning("Portrait Array at " + i + " is null.");
        }

        if (healthBarRect == null)
            Debug.LogWarning("CanvasStatus: No health bar object found");

        setHealth(100);
    }
	
	// Update is called once per frame
	void Update () {
        //Updating the health x scale with lerp.
        rate = 1 / Mathf.Abs(healthScaleToLerp - healthScale);
        scaleLerpValue = Mathf.Lerp(healthScale, healthScaleToLerp, Time.deltaTime * healthLerpSpeed * rate);
        if (float.IsNaN(scaleLerpValue))
            return;
        healthBarRect.localScale = new Vector3(scaleLerpValue, healthBarRect.localScale.y, healthBarRect.localScale.z);
        healthScale = healthBarRect.localScale.x;
    }

    //Health parameter: Int 0..100
    public void setHealth(int _health)
    {
        healthPoints = Mathf.Clamp(_health, 0, 100) / 100f;
        //Debug.Log("Health clamped: " + healthPoints);

        healthScale = healthBarRect.localScale.x;
        healthScaleToLerp = healthPoints;

        if (_health == 0)
            portrait.sprite = portraits[0];
        else if (_health > 0 && _health <= 33)
            portrait.sprite = portraits[1];
        else if (_health > 33 && _health <= 66)
            portrait.sprite = portraits[2];
        else if (_health > 66 && _health <= 100)
            portrait.sprite = portraits[3];
    }
}
