﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour {

    public int currentHealth;
    public int maxHealth = 100;
    public Animator animator; 

    public float walkSpeed = 5.0f;
    public float jumpForce = 3.0f;

    public int radiationDamage = 1;
    public float radiationSeconds = 1.0f;

    public bool isGrounded;

    public Transform projectilePrefab;

    private bool isFacingRight = true;
    private float distToGround;

    private Rigidbody2D rb2d;
    private AudioManager am;

    public Canvas gameOverCanvas;
    public IngameUI ingameUI;

    [Header("Events")]
    [Space]

    public UnityEvent OnLandEvent;

	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        am = AudioManager.instance;
        am.StopAllSounds();
        am.PlaySound("InGame");

        StartCoroutine(RadiationDamage());

        ResetPlayerAttributes();

        if (OnLandEvent == null)
            OnLandEvent = new UnityEvent();
    }

    void Update()
    {
        HandleMovement();
        HandleShooting();
        animator.SetFloat("speed",Mathf.Abs(rb2d.velocity.x));
        animator.SetBool("onAir", !isGrounded);
        
    }

    public void TakeDamage(int dmg)
    {
        currentHealth = Mathf.Max(currentHealth - Mathf.Max(dmg, 0), 0);
        animator.SetTrigger("gotDamage");

        if (currentHealth <= 0)
        {
            //TODO: Play death animation
            animator.SetTrigger("die");
            //On End of death animation
            //gameOverCanvas.gameObject.SetActive(true);
            //Destroy(this.gameObject);
            am.StopAllSounds();
            am.PlaySound("MCDeath");
            am.PlaySound("GameOver");
        }
        else
        {
            am.PlaySound("MCDamage");
        }

        ingameUI.setHealth(currentHealth);
        
    }

    private void TakeDamageRadiation(int dmg)
    {
        currentHealth = Mathf.Max(currentHealth - Mathf.Max(dmg, 0), 0);

        if (currentHealth <= 0)
        {
            //TODO: Play death animation
            Animator anim = gameObject.GetComponent<Animator>();
            anim.SetTrigger("die");
            //On End of death animation
            am.StopAllSounds();
            am.PlaySound("MCDeath");
            am.PlaySound("GameOver");
        }

        ingameUI.setHealth(currentHealth);
    }

    public void RecoverHealh(int health)
    {
        currentHealth = Mathf.Min(currentHealth + Mathf.Max(health, 0), maxHealth);
        am.PlaySound("MCLife");
    }

    void ResetPlayerAttributes ()
    {
        currentHealth = maxHealth;
        distToGround = GetComponent<BoxCollider2D>().bounds.extents.y;
    }

    private void HandleMovement()
    {
        float horizontalAxis = Input.GetAxis("Horizontal");
        float jumpAxis = (IsGrounded() ? Input.GetAxis("Jump") * jumpForce : rb2d.velocity.y);
        if (IsGrounded() && jumpAxis != rb2d.velocity.y)
        {
            am.PlaySound("MCJump");
        }

        // Set direction
        if (horizontalAxis > 0.01f)
            isFacingRight = true;
        else if (horizontalAxis < -0.01f)
            isFacingRight = false;
        UpdateDirection();

        Vector2 movement = new Vector2(horizontalAxis * walkSpeed, jumpAxis);

        rb2d.velocity = movement;
    }

    private void HandleShooting()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            animator.SetTrigger("attack");
            Transform projectile = Instantiate(projectilePrefab, transform.position, Quaternion.identity);
            Rigidbody2D projectile_rb2d = projectile.GetComponent<Rigidbody2D>();
            MC_Projectile projectileComponent = (MC_Projectile) projectile.GetComponent("MC_Projectile");
            projectile_rb2d.velocity = new Vector2(projectileComponent.speed * (isFacingRight ? 1.0f : -1.0f) , 0.0f);
            am.PlaySound("MCAttack");
        }
    }

    public void UpdateDirection()
    {
        if (transform.localScale.x >= 0.99f && !isFacingRight)
        {
            transform.localScale = new Vector3(
                -1.0f,  
                transform.localScale.y,
                transform.localScale.z);
        }
        else if (transform.localScale.x <= -0.99f && isFacingRight)
        {
            transform.localScale = new Vector3(
                1.0f,
                transform.localScale.y,
                transform.localScale.z);
        } 

    }

    public bool IsGrounded()
    {
        return isGrounded;
    }

    public void onLanding()
    {
        animator.SetBool("onAir", false);
    }

    public IEnumerator RadiationDamage()
    {
        while (true)
        {
            yield return new WaitForSeconds(radiationSeconds);
            TakeDamageRadiation(radiationDamage);
        }
        
    }

    public void GameOver()
    {
        rb2d.isKinematic = true;
        gameObject.GetComponent<Collider2D>().enabled = false;
        gameOverCanvas.gameObject.SetActive(true);
        Destroy(this.gameObject);
    }

}
