﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToMenuButton : MonoBehaviour {

    AudioManager am;

    void Start()
    {
        am = AudioManager.instance;
    }

    public void stop()
    {
        am.StopAllSounds();
        am.PlaySound("Menu");
    }
}
